﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerfectChannelShoppingBasket
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var inventory = new Inventory();
                var basketManager = new BasketManager(inventory);
                var checkout = new Checkout(inventory, basketManager);

                Console.WriteLine("Welcome to the store!");
                WriteInstructions();

                while (true)
                {
                    WaitForInput(inventory, basketManager, checkout);
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Something went really wrong!  Please exit the application and try again.");
                Console.ReadLine();
            }
        }

        private static void WriteInstructions()
        {
            Console.WriteLine("Type 'list' to see a list of items.");
            Console.WriteLine("Add a product to your basket by typing in the following format:  '[Basket Name] [Product Name/Id] [Amount]'.");
            Console.WriteLine("For example, 'mybasket Apples 3' will add 3 apples to 'mybasket'.");
            Console.WriteLine("Type 'checkout [Basket Name]' to check out.");
            Console.WriteLine("Instructions are case-sensitive.");
        }

        private static void WaitForInput(Inventory inventory, BasketManager basketManager, Checkout checkout)
        {
            var result = Console.ReadLine();

            var splitResult = result.Split(' ');

            if (result == "list")
            {
                WriteProductList(inventory.List());
                return;
            }
            else if (splitResult.Count() == 2 && splitResult[0] == "checkout")
            {
                try
                {
                    var invoice = checkout.Process(splitResult[1]);
                    WriteProductList(invoice);
                    var totalCost = invoice.Sum(x => x.TotalCost);
                    Console.WriteLine($"Total: {totalCost}");
                }
                catch (InvalidOperationException e)
                {
                    Console.WriteLine("Something went wrong when checking out!");
                    Console.WriteLine(e.Message);
                }

                return;
            }
            else if (splitResult.Count() == 3)
            {
                var basketName = splitResult[0];
                var productName = splitResult[1];
                var productId = 0;
                var isNumericId = int.TryParse(productName, out productId);
                var count = 0;
                var canDetermineCount = int.TryParse(splitResult[2], out count);

                if (!canDetermineCount)
                {
                    Console.WriteLine("Couldn't parse the amount, please try again.");
                    return;
                }

                try
                {
                    var updatedBasket = isNumericId ?
                        basketManager.AddItem(basketName, productId, count)
                        : basketManager.AddItem(basketName, productName, count);

                    Console.WriteLine("Item was added to your basket.  Your basket is now:");
                    WriteProductList(updatedBasket);
                }
                catch (InvalidOperationException e)
                {
                    Console.WriteLine("An error occurred:");
                    Console.WriteLine(e.Message);
                    return;
                }
            }
            else
            {
                Console.WriteLine("Couldn't understand the input format.  Please try again.");
            }
        }

        private static void WriteProductList(IEnumerable<ProductCount> list)
        {
            foreach (var item in list)
            {
                Console.WriteLine($"Id: {item.Product.Id}, Name: {item.Product.Name}, Description: {item.Product.Description}, Stock: {item.Count}");
            }
        }

        private static void WriteProductList(IEnumerable<InvoiceItem> list)
        {
            foreach (var item in list)
            {
                Console.WriteLine($"{item.Item.Count} {item.Item.Product.Name} at {item.Item.Product.Price} = {item.TotalCost}");
            }
        }
    }
}
