﻿namespace PerfectChannelShoppingBasket
{
    public class InvoiceItem
    {
        public InvoiceItem(ProductCount item)
        {
            this.Item = item;
        }

        public ProductCount Item { get; set; }

        public decimal TotalCost
        {
            get
            {
                return this.Item.Product.Price * this.Item.Count;
            }
        }
    }
}
