﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerfectChannelShoppingBasket
{
    public class Inventory
    {
        private IEnumerable<ProductCount> inventory = new List<ProductCount>
        {
            new ProductCount { Product = new Product { Id = 1, Name = "Apples", Description = "Fruit", Price = 2.5m }, Count = 5 },
            new ProductCount { Product = new Product { Id = 2, Name = "Bread", Description = "Loaf", Price = 1.35m }, Count = 10 },
            new ProductCount { Product = new Product { Id = 3, Name = "Oranges", Description = "Fruit", Price = 2.99m }, Count = 10 },
            new ProductCount { Product = new Product { Id = 4, Name = "Milk", Description = "Milk", Price = 2.07m }, Count = 3 },
            new ProductCount { Product = new Product { Id = 5, Name = "Chocolate", Description = "Bars", Price = 1.79m }, Count = 20 },
        };

        public IEnumerable<ProductCount> List()
        {
            return this.inventory;
        }

        public Product GetProduct(string name, int count = 1)
        {
            var product = this.Find(name).Product;
            this.AssertCanTakeAmount(product, count);
            return product;
        }

        public Product GetProduct(int id, int count = 1)
        {
            var product = this.Find(id).Product;
            this.AssertCanTakeAmount(product, count);
            return product;
        }

        public void AssertCanTakeAmount(Product product, int count)
        {
            var productInStock = this.inventory.Single(x => x.Product.Id == product.Id);
            if (productInStock.Count < count)
            {
                throw new InvalidOperationException($"Can't take {count} {productInStock.Product.Name} because there are only {productInStock.Count} left!  Please try again.");
            }
        }

        public void Take(int id, int count)
        {
            var item = this.Find(id);
            this.AssertCanTakeAmount(item.Product, count);
            item.Count = item.Count - count;
        }

        private ProductCount Find(string name)
        {
            var item = this.inventory.SingleOrDefault(x => x.Product.Name == name);

            if (item == null)
            {
                throw new InvalidOperationException($"Can't find an item with the name {name}!  Please try again.");
            }

            return item;
        }

        private ProductCount Find(int id)
        {
            var item = this.inventory.SingleOrDefault(x => x.Product.Id == id);

            if (item == null)
            {
                throw new InvalidOperationException($"Can't find an item with the id {id}!  Please try again.");
            }

            return item;
        }
    }
}
