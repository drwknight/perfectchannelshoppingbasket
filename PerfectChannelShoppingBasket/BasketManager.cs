﻿using System.Collections.Generic;
using System.Linq;

namespace PerfectChannelShoppingBasket
{
    public class BasketManager
    {
        private Dictionary<string, List<ProductCount>> baskets = new Dictionary<string, List<ProductCount>>();
        private Inventory inventory;

        public BasketManager(Inventory inventory)
        {
            this.inventory = inventory;
        }

        public IEnumerable<ProductCount> AddItem(string userName, string itemName, int count = 1)
        {
            var basket = this.GetOrCreateBasket(userName);
            var startingProductCount = basket.SingleOrDefault(x => x.Product.Name == itemName) ?? new ProductCount(this.inventory.GetProduct(itemName), 0);
            var totalCount = startingProductCount.Count + count;
            this.inventory.AssertCanTakeAmount(startingProductCount.Product, totalCount);
            return this.UpdateBasket(basket, startingProductCount.Product, totalCount);
        }

        public IEnumerable<ProductCount> AddItem(string userName, int itemId, int count = 1)
        {
            var basket = this.GetOrCreateBasket(userName);
            var startingProductCount = basket.SingleOrDefault(x => x.Product.Id == itemId) ?? new ProductCount(this.inventory.GetProduct(itemId), 0);
            var totalCount = startingProductCount.Count + count;
            this.inventory.AssertCanTakeAmount(startingProductCount.Product, totalCount);
            return this.UpdateBasket(basket, startingProductCount.Product, totalCount);
        }

        public List<ProductCount> GetBasket(string userName)
        {
            return this.GetOrCreateBasket(userName);
        }

        private List<ProductCount> UpdateBasket(List<ProductCount> basket, Product productToAdd, int totalCount)
        {
            var productInBasket = basket.SingleOrDefault(x => x.Product.Id == productToAdd.Id);
            if (productInBasket != null)
            {
                productInBasket.Count = totalCount;
            }
            else
            {
                basket.Add(new ProductCount { Product = productToAdd, Count = totalCount });
            }

            return basket;
        }

        private List<ProductCount> GetOrCreateBasket(string userName)
        {
            if (!this.baskets.ContainsKey(userName))
            {
                this.baskets.Add(userName, new List<ProductCount>());
            }

            return this.baskets[userName];
        }
    }
}
