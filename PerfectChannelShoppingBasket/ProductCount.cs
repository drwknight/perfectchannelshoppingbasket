﻿namespace PerfectChannelShoppingBasket
{
    public class ProductCount
    {
        public ProductCount()
        {
        }

        public ProductCount(Product product, int count)
        {
            this.Product = product;
            this.Count = count;
        }

        public Product Product { get; set; }
        public int Count { get; set; }
    }
}
