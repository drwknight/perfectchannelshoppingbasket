﻿In answer to the test questions:
-       How long did you spend on your solution?
		A little over 3 hours.

-       How do you compile and run your solution?
		This is a console application so should run pretty easily.  You will need to restore nuget packages to run the tests (and have a way to run NUnit tests).

-       What assumptions did you make when went implementing your solution
		That functionality should be basic.
		That it is acceptable to have concrete classes as dependencies - usually I would define them as interfaces, but didn't here to save time.

-       Why did you pick the language and design that you did?
		I'm more familiar with C#.
		I decided to do TDD with this, in order to ensure all the key points of functionality were met.
		Application structure flowed more from that - I knew there would be simple data objects like Products and more complex, service-like classes like the BasketManager.
		I mostly tried to keep classes Domain-oriented, for example the Inventory being responsible for managing its own collection of products.

-       If you were unable to complete any user stories, outline why and how would you have liked to implement them
		User Story 3, acceptance criteria 2:  Request to add a *list* of items
		This would be difficult to do in a console application because the input could get cumbersome.  
		Once the input is sorted, though, it should be simple to change - simply iterate through the list of items provided and call the AddItem function for each.
