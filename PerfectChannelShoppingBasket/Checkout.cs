﻿using System.Collections.Generic;
using System.Linq;

namespace PerfectChannelShoppingBasket
{
    public class Checkout
    {
        private Inventory inventory;
        private BasketManager basketManager;

        public Checkout(Inventory inventory, BasketManager basketManager)
        {
            this.inventory = inventory;
            this.basketManager = basketManager;
        }

        public IEnumerable<InvoiceItem> Process(string basketName)
        {
            var basket = this.basketManager.GetBasket(basketName);

            // The issue here is that we don't check stock levels before processing, only during.
            // This means that we could get halfway through before finding out that there's not enough.
            // This can happen if more than one user purchases/tries to purchase the same product.
            foreach (var item in basket)
            {
                this.inventory.Take(item.Product.Id, item.Count);
            }

            return basket.Select(x => new InvoiceItem(x));
        }
    }
}
