﻿using System;
using System.Linq;
using NUnit.Framework;
using PerfectChannelShoppingBasket;
using Shouldly;

namespace ShoppingBasket.Tests
{
    /// <summary>
    /// "As a shopper, I want to add an item to my basket, so that I can purchase it later."
    /// 1.    Run application
    /// 2.    Request to add an item by name or identifier for a specific user's basket.
    /// 3.    Returns the user's basket.
    /// 4.    To access user's basket, request the basket by user name.
    /// 5.    Users should not be able to add items which are not in stock.
    /// </summary>
    [TestFixture]
    public class UserStory2
    {
        /// <summary>
        /// Acceptance criteria 1-2 ("request to add an item by name").
        /// </summary>
        [Test]
        public void WhenShopperAddsItemByName_ThenItemIsAddedToBasket()
        {
            var inventory = new Inventory();
            var basketManager = new BasketManager(inventory);
            var result = basketManager.AddItem("user1", "Apples");
            result.Count().ShouldBe(1);
            var itemInBasket = result.First();
            itemInBasket.Product.Name.ShouldBe("Apples");
            itemInBasket.Count.ShouldBe(1);
        }

        /// <summary>
        /// Acceptance criteria 1-2 ("request to add an item by identifier").
        /// </summary>
        [Test]
        public void WhenShopperAddsItemById_ThenItemIsAddedToBasket()
        {
            var inventory = new Inventory();
            var basketManager = new BasketManager(inventory);
            var result = basketManager.AddItem("user1", 1);
            result.Count().ShouldBe(1);
            var itemInBasket = result.First();
            itemInBasket.Product.Id.ShouldBe(1);
            itemInBasket.Count.ShouldBe(1);
        }

        /// <summary>
        /// Not strictly in the requirements, but it follows that being unable to find an item should prevent any attempt at further processing.
        /// </summary>
        [Test]
        public void WhenShopperAddsItemThatDoesNotExist_ThenStopFurtherProcessing()
        {
            var inventory = new Inventory();
            var basketManager = new BasketManager(inventory);
            Assert.Throws<InvalidOperationException>(() => basketManager.AddItem("user1", 100));
        }

        /// <summary>
        /// Acceptance criteria 3, returning the basket - this test is to ensure that the whole basket is returned, not just a list with the added item.
        /// </summary>
        [Test]
        public void WhenShopperAddsItemToBasket_ThenWholeBasketIsReturned()
        {
            var inventory = new Inventory();
            var basketManager = new BasketManager(inventory);
            var result = basketManager.AddItem("user1", 1);
            var result2 = basketManager.AddItem("user1", 2);
            result.Count().ShouldBe(2);
            var firstItem = result.ElementAt(0);
            firstItem.Product.Id.ShouldBe(1);
            firstItem.Count.ShouldBe(1);
            var secondItem = result.ElementAt(1);
            secondItem.Product.Id.ShouldBe(2);
            secondItem.Count.ShouldBe(1);
        }

        /// <summary>
        /// While not explicitly stated in the requirements, it is logical that attempting to add the same item will increase the count, not add a different ProductCount.
        /// </summary>
        [Test]
        public void WhenShopperAddsItemToBasketThatAlreadyExists_ThenItemCountIsUpdated()
        {
            var inventory = new Inventory();
            var basketManager = new BasketManager(inventory);
            var result = basketManager.AddItem("user1", 1);
            var result2 = basketManager.AddItem("user1", 1);
            result.Count().ShouldBe(1);
            var firstItem = result.First();
            firstItem.Product.Id.ShouldBe(1);
            firstItem.Count.ShouldBe(2);
        }

        /// <summary>
        /// Acceptance criteria 4.  "To access user's basket, request the basket by user name."
        /// </summary>
        [Test]
        public void WhenShopperRequestsBasketByUserName_ThenReturnBasket()
        {
            var inventory = new Inventory();
            var basketManager = new BasketManager(inventory);
            basketManager.AddItem("user1", 1);
            var result = basketManager.GetBasket("user1");
            var firstItem = result.First();
            firstItem.Product.Id.ShouldBe(1);
            firstItem.Count.ShouldBe(1);
        }

        /// <summary>
        /// Acceptance criteria 5.  "Users should not be able to add items which are not in stock."
        /// </summary>
        [Test]
        public void WhenProductNotInStock_ThenShopperShouldNotBeAbleToAddToBasket()
        {
            var inventory = new Inventory();
            var basketManager = new BasketManager(inventory);
            Assert.Throws<InvalidOperationException>(() => basketManager.AddItem("user1", 1, 100));
        }
    }
}
