﻿using System.Linq;
using NUnit.Framework;
using PerfectChannelShoppingBasket;
using Shouldly;

namespace ShoppingBasket.Tests
{
    /// <summary>
    /// "As a shopper, I want to add items of different quantity to my basket."
    /// 1.    Run application 
    /// 2.    Request to add a list of items, specifying their quantity.
    /// 3.    Returns users updated basket.
    /// </summary>
    [TestFixture]
    public class UserStory3
    {
        /// <summary>
        /// 
        /// </summary>
        [Test]
        public void WhenShopperAddsMultipleItemsToBasket_ThenUpdateBasketAccordingly()
        {
            var inventory = new Inventory();
            var basketManager = new BasketManager(inventory);
            var result = basketManager.AddItem("user1", 1, 3);
            var basketItem = result.First();
            basketItem.Product.Id.ShouldBe(1);
            basketItem.Count.ShouldBe(3);
        }
    }
}
