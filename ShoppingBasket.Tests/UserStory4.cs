﻿using System;
using System.Linq;
using NUnit.Framework;
using PerfectChannelShoppingBasket;
using Shouldly;

namespace ShoppingBasket.Tests
{
    /// <summary>
    /// "As a shopper, I want to check out my basket of items, so that I can own them."
    /// 1.    Run application
    /// 2.    Request to checkout a user's basket by name
    /// 3.    Return invoice of items.
    /// 4.    When requesting list of items, stock should be reduced accordingly
    /// 5.    Stock should not be allowed to be negative
    /// </summary>
    [TestFixture]
    public class UserStory4
    {
        /// <summary>
        /// Acceptance criteria 2-3 "Request to checkout a user's basket by name", "Return invoice of items."
        /// </summary>
        [Test]
        public void WhenShopperChecksOut_ThenReturnInvoiceOfItems()
        {
            var inventory = new Inventory();
            var basketManager = new BasketManager(inventory);
            basketManager.AddItem("user1", 1, 2);
            basketManager.AddItem("user1", 2, 1);
            var checkout = new Checkout(inventory, basketManager);
            var result = checkout.Process("user1");
            result.Count().ShouldBe(2);
            var firstItem = result.ElementAt(0);
            firstItem.Item.Product.Id.ShouldBe(1);
            firstItem.Item.Count.ShouldBe(2);
            var secondItem = result.ElementAt(1);
            secondItem.Item.Product.Id.ShouldBe(2);
            secondItem.Item.Count.ShouldBe(1);
        }

        /// <summary>
        /// Acceptance criteria 4 "When requesting list of items, stock should be reduced accordingly."
        /// </summary>
        [Test]
        public void WhenShopperChecksOut_ThenReduceInventoryStock()
        {
            var inventory = new Inventory();
            var startingItemCount = inventory.List().Single(x => x.Product.Id == 1).Count;
            var basketManager = new BasketManager(inventory);
            basketManager.AddItem("user1", 1, 2);
            var checkout = new Checkout(inventory, basketManager);
            var result = checkout.Process("user1");
            var newCount = inventory.List().Single(x => x.Product.Id == 1);
            newCount.Count.ShouldBe(startingItemCount - 2);
        }

        /// <summary>
        /// Acceptance criteria 5 " Stock should not be allowed to be negative."
        /// </summary>
        [Test]
        public void WhenInventoryReduced_ThenStockCannotBeNegative()
        {
            var inventory = new Inventory();
            var startingItemCount = inventory.List().Single(x => x.Product.Id == 1).Count;
            var basketManager = new BasketManager(inventory);
            basketManager.AddItem("user1", 1, 1);
            var checkout = new Checkout(inventory, basketManager);
            inventory.List().Single(x => x.Product.Id == 1).Count = 0;
            Assert.Throws<InvalidOperationException>(() => checkout.Process("user1"));
        }
    }
}
