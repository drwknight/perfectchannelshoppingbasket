﻿using System.Linq;
using NUnit.Framework;
using PerfectChannelShoppingBasket;
using Shouldly;

namespace ShoppingBasket.Tests
{
    /// <summary>
    /// "As a shopper, I want to be able to see a list of all possible items available, so that I can determine what I can buy."
    /// 1.    Run application 
    /// 2.    Request a “list” of items
    /// 3.    Returns each item detailing Name, Description, Identifier and stock quantity.
    /// </summary>
    [TestFixture]
    public class UserStory1
    {
        /// <summary>
        /// Acceptance criteria 1-3.
        /// </summary>
        [Test]
        public void WhenShopperRequestsInventory_ThenReturnListOfItems()
        {
            var inventory = new Inventory();
            var result = inventory.List();
            result.Count().ShouldBeGreaterThan(0);
            var firstItem = result.First();
            firstItem.Product.Name.ShouldNotBeEmpty();
            firstItem.Product.Description.ShouldNotBeEmpty();
            firstItem.Product.Id.ShouldBeGreaterThan(0);
            firstItem.Count.ShouldBeGreaterThan(0);
        }
    }
}
